package com.queiroz.josias.cellfiles.activities

import android.app.Activity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import com.queiroz.josias.cellfiles.R
import com.queiroz.josias.cellfiles.`interface`.CallBack
import com.queiroz.josias.cellfiles.enum.States

class ServerActivity : Activity(),CallBack {

    lateinit var mlabelStatus: TextView
    lateinit var mlabelNomeArquivo:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_server)

        mlabelStatus = findViewById(R.id.label_status)
        mlabelNomeArquivo = findViewById(R.id.label_nome_arquivo)

        mlabelStatus.text = "status: ${States.PARADO}"
    }

    override fun updateNome(state: States, name: String) {
        runOnUiThread {
            mlabelStatus.text = "status: ${state}"
            mlabelNomeArquivo.text = name
        }
    }

    override fun transferencia(state: States) {
        runOnUiThread {
            mlabelStatus.text = "status: ${state}"
        }
    }

    override fun finalizando(state: States) {
        val msg = if(mlabelStatus.text == States.RECEBENDO) "${mlabelNomeArquivo.text} recebido com sucesso" else "A transferência não aconteceu"
        runOnUiThread {
            mlabelStatus.text = "status: ${state}"
        }
        Toast.makeText(this, msg, Toast.LENGTH_LONG)
        finish()
    }
}
