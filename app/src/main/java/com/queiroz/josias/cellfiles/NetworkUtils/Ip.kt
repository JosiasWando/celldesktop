package com.queiroz.josias.cellfiles.NetworkUtils

import java.net.InetAddress
import java.net.NetworkInterface
import java.util.*

object Ip {
    fun getlocalhostIp(): String?{
        val networkInterfaces = NetworkInterface.getNetworkInterfaces()
        for (intf in networkInterfaces){
            val iplist: List<InetAddress> = Collections.list(intf.inetAddresses)
            for (adress in iplist){
                if(adress.isLoopbackAddress) return adress.hostAddress
            }
        }

        return null
    }
}