package com.queiroz.josias.cellfiles.NetworkUtils
import com.queiroz.josias.cellfiles.`interface`.CallBack
import com.queiroz.josias.cellfiles.enum.States
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.net.ServerSocket
import java.net.Socket

class SocketApi(val feddbackCliente: CallBack): Runnable{

    val server = ServerSocket(7077)
    lateinit var entrada: InputStream
    lateinit var saida: OutputStream
    lateinit var conexao: Socket
    val bufferChunck = 1024
    var buffer = ByteArray(bufferChunck)

    fun startService(){
        conexao = server.accept()
        entrada = conexao.getInputStream()
        saida = conexao.getOutputStream()

        var size = entrada.read(buffer)
        val bFileName = buffer.copyOf(size)
        val fileName = bFileName.toString(Charsets.UTF_8)
        feddbackCliente.updateNome(States.RECEBENDO, fileName)
        buffer = ByteArray(bufferChunck)
        transferFile(fileName)

    }

    fun transferFile(fileName:String){
        val file = File("/data/sends/${fileName}")
        val filedir = File("/data/sends")

        if (!file.exists()){
            if(!filedir.exists()) filedir.mkdir()
            file.createNewFile()
            val msg = "Metadados Chegaram".toByteArray(Charsets.UTF_8)
            saida.write(msg)
            saida.flush()
            feddbackCliente.transferencia(States.RECEBENDO)
        }else{
            val msg = "Arquivo existe".toByteArray(Charsets.UTF_8)
            saida.write(msg)
            saida.flush()
            feddbackCliente.transferencia(States.EXISTENTE)
            return
        }

        while (entrada.read(buffer) > 0){
            file.appendBytes(buffer)
            buffer = ByteArray(bufferChunck)
        }
    }

    override fun run() {
        startService()
        feddbackCliente.finalizando(States.FINALIZADO)
    }


}