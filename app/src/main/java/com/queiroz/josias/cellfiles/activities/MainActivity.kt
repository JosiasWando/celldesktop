package com.queiroz.josias.cellfiles.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.queiroz.josias.cellfiles.NetworkUtils.Ip
import com.queiroz.josias.cellfiles.R

class MainActivity : Activity(), View.OnClickListener {


    lateinit var mLabelIp: TextView
    lateinit var mBtnStartSock: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mLabelIp = findViewById(R.id.lblIp)
        mLabelIp.text = Ip.getlocalhostIp()

        mBtnStartSock = findViewById(R.id.btnStartSock)
        mBtnStartSock.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
      when(view?.id){
          R.id.btnStartSock -> startServer()
      }
    }

    fun startServer(){
        Toast.makeText(this,"O evento de clique estar sendo capturado.", Toast.LENGTH_SHORT).show()
        val serverScreen = Intent(this, ServerActivity::class.java)
        startActivity(serverScreen)
    }
}
