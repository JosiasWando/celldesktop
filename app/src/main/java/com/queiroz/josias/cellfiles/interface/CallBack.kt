package com.queiroz.josias.cellfiles.`interface`

import com.queiroz.josias.cellfiles.enum.States

interface CallBack {
    fun updateNome(state: States, name: String)

    fun transferencia(state: States)

    fun finalizando(state: States)
}