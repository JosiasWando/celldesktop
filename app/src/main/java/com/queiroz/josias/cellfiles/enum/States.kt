package com.queiroz.josias.cellfiles.enum

enum class States{
    PARADO, RECEBENDO, EXISTENTE, FINALIZADO
}